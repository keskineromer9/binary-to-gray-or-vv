# Binary to Gray or VV for Logisim

Experiment-2.circ file has two basic circuits: Left one converts Gray Code to Binary Code 
and right one converts Binary Code to Gray Code.

You must have downloaded Logisim to use this.
You can donwload it from https://sourceforge.net/projects/circuit/. 

Also you need to download Java to use Logisim.
You can download it in Linux/Ubuntu by following these steps:

# Open Terminal
1) Write "sudo apt update"
2) Then write/copy "sudo apt install default-jdk"
Press "y" if you want to continue

# Install OpenJRE11
1) Write "sudo apt update"
2) Write "sudo apt install default-jre"

# Install ORACLE Java 17 
1) Write "sudo apt install software-properties-common"
2) Write "sudo add-apt-repository ppa:linuxuprising/java -y"
3) Write "sudo apt update"
4) Write "sudo apt install oracle-java17-installer"
Press TAB to select <Ok> and press Enter to confirm if you agree to the
terms of the license agreement.

The wizard prompts you to accept the Oracle license terms to continue with the installation.
Press TAB to select <Yes> and Enter to confirm.

After accepting the terms, the installer completes the Java installation.

# Set Version as the Default
1) Write "sudo apt install oracle-java17-set-default"

Now you can use Logisim by right-clicking to the java icon and click on the "Open with Other Application".

To use this circuit, open Logisim and click File->Open-> <wherever your "Experiment-2.circ" is> directory.
